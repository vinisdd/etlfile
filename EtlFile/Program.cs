﻿using EtlFile.Helper;
using EtlFile.Services;
using System;
using System.Threading;

namespace EtlFile
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var fileHelper = new FileHelper();
            var processor = new ProcessorEtl(fileHelper);
            Console.WriteLine("wait for new files");
            while (true)
            {
                processor.ProcessFiles();
                //Execute every 5 minutes
                Thread.Sleep(new TimeSpan(0, 5, 0));
                Console.Clear();
                Console.WriteLine("wait for new files");
            }
        }

    }
}

﻿using EtlFile.Entity;
using EtlFile.Helper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EtlFile.Services
{
    public class ProcessorEtl
    {
        private readonly FileHelper _fileHelper;

        public ProcessorEtl(FileHelper fileHelper)
        {
            _fileHelper = fileHelper;
        }

        public void ProcessFiles()
        {
            var salesmans = new List<Salesman>();
            var clients = new List<Client>();
            var sales = new List<Sale>();

            foreach (var (_, lines) in _fileHelper.GetInputFileLines())
            {
                foreach (var line in lines)
                {
                    if (line.Length == 0)
                    {
                        continue;
                    }

                    try
                    {
                        var recordType = line.Substring(0, 3);

                        switch (recordType)
                        {
                            case "001":
                                salesmans.Add(new Salesman(line));
                                break;
                            case "002":
                                clients.Add(new Client(line));
                                break;
                            case "003":
                                sales.Add(new Sale(line));
                                break;
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine($"erro na linha {line}");
                    }

                }
            }

            if (!(salesmans.Any() && clients.Any() && sales.Any()))
            {
                return;
            }

            GenerateSummarizedOutput($"files_processed_at_{DateTime.Now:dd_MM_yyyy}", sales, clients, salesmans);
        }

        private void GenerateSummarizedOutput(string fullPathFileName, List<Sale> sales, List<Client> clients, List<Salesman> salesmans)
        {
            var outputLines = new List<string>();

            var mostExpressiveSale = Sale.GetMostExpresive(sales);
            var worstSalesmanEver = Sale.GetWorstSalesmanNameEver(sales);

            outputLines.Add($"Amount of clients: {clients.Count()}");
            outputLines.Add($"Amount of salesman: {salesmans.Count()}");
            outputLines.Add($"ID of the most expensive sale: {mostExpressiveSale?.Id}");
            outputLines.Add($"Worst salesman ever: {worstSalesmanEver}");

            _fileHelper.WriteOutFile(fullPathFileName, outputLines);

            outputLines.ForEach(Console.WriteLine);
        }
    }
}

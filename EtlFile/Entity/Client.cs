﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EtlFile.Entity
{
    public class Client
    {
        public string Document { get; set; }
        public string Name { get; set; }
        public string BusinessArea { get; set; }

        public Client(string line)
        {
            var splitedLine = line.Split("ç");
            Document = splitedLine[1];
            Name = splitedLine[2];
            BusinessArea = splitedLine[3];
        }
    }
}

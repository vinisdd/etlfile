﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EtlFile.Entity
{
    public class Salesman
    {
        public string Name { get; set; }
        public string Document { get; set; }
        public double Salary { get; set; }

        public Salesman(string line)
        {
            var splitedLine = line.Split("ç");
            Name = splitedLine[2];
            Document = splitedLine[1];
            Salary = double.Parse(splitedLine[3]);
        }
    }
}

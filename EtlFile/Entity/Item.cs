﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EtlFile.Entity
{
    public class Item
    {
        public int Id { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }

        public double GetTotalPrice() => Price * Quantity;

        public static List<Item> GetItemsFromLine(string line)
        {
            var arr = line.Split(new[] { '[', ']', ',' },
                StringSplitOptions.RemoveEmptyEntries);

            var ids = arr[0].Split('-');
            var qtds = arr[1].Split('-');
            var prices = arr[2].Split('-');

            return ids
                .Select((t, i) =>
                    new Item
                    {
                        Id = int.Parse(t),
                        Quantity = double.Parse(qtds[i]),
                        Price = double.Parse(prices[i])
                    }).ToList();

        }


    }
}

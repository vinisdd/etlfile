﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EtlFile.Entity
{
    public class Sale
    {
        public int Id { get; set; }        
        public List<Item> Itens { get; set; }
        public string SalesmanName { get; set; }

        public double TotalPriceSale() => Itens.Sum(i=>i.GetTotalPrice());

        public Sale()
        {

        }

        public Sale(string line)
        {
            var splitedLine = line.Split("ç");
            Id = int.Parse(splitedLine[1]);
            Itens = Item.GetItemsFromLine(splitedLine[2]);
            SalesmanName = splitedLine[3];
        }
        
        public static string GetWorstSalesmanNameEver(List<Sale> sales)
        {
            return sales.GroupBy(sale => sale.SalesmanName)
                    .Select(group => new { SalesmanName = group.Key, TotalSold = group.Sum(i => i.TotalPriceSale()) })
                    .OrderBy(a => a.TotalSold)
                    .FirstOrDefault()?
                    .SalesmanName;
        }

        public static Sale GetMostExpresive(List<Sale> sales)
        {
            return sales
                .OrderByDescending(a => a.Itens.Sum(i => i.GetTotalPrice()))
                .FirstOrDefault();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace EtlFile.Helper
{
    public class FileHelper
    {

        //Readonly propertys
        private readonly string _inputFilePath = $"data{Path.DirectorySeparatorChar}in";
        private readonly string _outputFilePath = $"data{Path.DirectorySeparatorChar}out";
        private readonly string _fileExtession = ".dat";
        //was not tested in linux because I did not have an environment (new laptop)
        private readonly string _homeFullPath = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ?
            Environment.ExpandEnvironmentVariables("%USERPROFILE%/")
            : Environment.ExpandEnvironmentVariables("%HOME%/");

        private string InputPath => $"{_homeFullPath}{_inputFilePath}";
        private string OutputPath => $"{_homeFullPath}{_outputFilePath}";

        private readonly Dictionary<string, List<string>> _executedsFiles = new Dictionary<string, List<string>>();

        private string GetOutputFullPath(string fileName) => $"{OutputPath}{Path.DirectorySeparatorChar}{fileName}";

        public FileHelper()
        {
            InitFiles();
        }

        private void InitFiles()
        {
            if (!Directory.Exists(InputPath))
            {
                Directory.CreateDirectory(InputPath);
            }

            if (!Directory.Exists(OutputPath))
            {
                Directory.CreateDirectory(OutputPath);
            }
        }

        public Dictionary<string, List<string>> GetInputFileLines()
        {
            var newFiles = new Dictionary<string, List<string>>();
            foreach (var file in Directory.EnumerateFiles(InputPath, $"*{_fileExtession}"))
            {
                if (!_executedsFiles.ContainsKey(file))
                {
                    _executedsFiles[file] = File.ReadAllLines(file, System.Text.Encoding.UTF8).ToList();
                    newFiles[file] = _executedsFiles[file];
                }
            }
            return newFiles;
        }

        public void WriteOutFile(string file, List<string> lines)
        {
            var fileName = file
                 .Split(Path.DirectorySeparatorChar)
                 .LastOrDefault()
                 ?.Replace(_fileExtession, "");

            var doneFileName = $"{fileName}.done{_fileExtession}";

            var fullPath = GetOutputFullPath(doneFileName);
            if (!File.Exists(fullPath))
            {
                File.Create(fullPath)
                    .Dispose();
            }

            File.WriteAllLines(fullPath, lines);
        }


    }
}

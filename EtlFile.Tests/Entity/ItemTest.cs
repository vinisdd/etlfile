﻿using EtlFile.Entity;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace EtlFile.Tests.Entity
{
    public class ItemTest
    {
        [Test]
        public void ShouldItemHavePriceAndQuantityReturnValue()
        {
            var item = new Item()
            {
                Price = 1,
                Quantity = 10
            };

            Assert.AreEqual(10, item.GetTotalPrice());
        }
    }
}

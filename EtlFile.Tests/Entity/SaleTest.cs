﻿using EtlFile.Entity;
using NUnit.Framework;
using System.Collections.Generic;

namespace EtlFile.Tests.Entity
{
    public class SaleTest
    {
        [Test]
        public void ShouldSaleHaveItensReturnTotalPrice()
        {
            var sale = new Sale()
            {
                Itens = new List<Item>()
                {
                    new Item()
                    {
                        Price = 1,
                        Quantity = 10
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 10
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 10
                    }
                }
            };

            Assert.AreEqual(30, sale.TotalPriceSale());
        }

        [Test]
        public void ShouldSalesWithDiferentSalesmanReturnWorst()
        {
            var sale = new Sale()
            {
                SalesmanName = "best",
                Itens = new List<Item>()
                {
                    new Item()
                    {
                        Price = 1,
                        Quantity = 10
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 10
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 10
                    }
                }
            };
            var sale2 = new Sale()
            {
                SalesmanName = "Worst",
                Itens = new List<Item>()
                {
                    new Item()
                    {
                        Price = 1,
                        Quantity = 4
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 3
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 1
                    }
                }
            };
            var worst = Sale.GetWorstSalesmanNameEver(new List<Sale>() { sale, sale2 });
            Assert.AreEqual(sale2.SalesmanName, worst);
        }

        [Test]
        public void ShoudSalesWithDiferentValuesGetMostExpresive()
        {
            var sale = new Sale()
            {
                SalesmanName = "best",
                Itens = new List<Item>()
                {
                    new Item()
                    {
                        Price = 1,
                        Quantity = 10
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 10
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 10
                    }
                }
            };
            var sale2 = new Sale()
            {
                SalesmanName = "Worst",
                Itens = new List<Item>()
                {
                    new Item()
                    {
                        Price = 1,
                        Quantity = 4
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 3
                    },
                    new Item()
                    {
                        Price = 1,
                        Quantity = 1
                    }
                }
            };
            var most = Sale.GetMostExpresive(new List<Sale>() { sale2, sale });

            Assert.AreEqual(sale.Id, most.Id);

        }
    }
}
